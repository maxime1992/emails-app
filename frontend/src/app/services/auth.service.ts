import { Observable } from 'rxjs';
import { map, tap, mapTo } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Token } from '../interfaces/token.interface';
import { EmailsService } from './emails.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private token: Token;

  constructor(
    private httpClient: HttpClient,
    private emailsService: EmailsService,
  ) {}

  logIn(email: string, password: string): Observable<null> {
    return this.httpClient
      .post<Token>('http://localhost:3856/auth/token', {
        email,
        password,
      })
      .pipe(
        tap(token => {
          this.token = token;
        }),
        mapTo(null),
      );
  }

  logOut() {
    this.cleanUpAllServices();
  }

  cleanUpAllServices() {
    this.token = null;
    this.emailsService.cleanUp();
  }

  getToken(): Token {
    return this.token;
  }
}
