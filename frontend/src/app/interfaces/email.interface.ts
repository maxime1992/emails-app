import { User } from './user.interface';

export interface NewEmail {
  subject: string;
  toEmail: string;
  body: string;
}

export interface Email extends NewEmail {
  id: string;
}
