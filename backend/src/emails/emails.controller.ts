import { Usr } from './../users/user.decorator';
import {
  Controller,
  Post,
  Body,
  UseGuards,
  HttpException,
  HttpStatus,
  Get,
} from '@nestjs/common';
import { EmailsService } from './emails.service';
import { SendEmailDto, Email } from './email.entity';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../users/user.entity';

@Controller('emails')
@UseGuards(AuthGuard('jwt'))
export class EmailsController {
  constructor(private emailsService: EmailsService) {}

  @Post()
  async sendEmail(@Usr() user: User, @Body() email: SendEmailDto) {
    const emailFound: Email = await this.emailsService.getEmailById(email.id);

    if (!!emailFound) {
      throw new HttpException(
        `Email with ID "${email.id}" has already been sent`,
        HttpStatus.CONFLICT
      );
    }

    const preparedEmail: Email = {
      ...email,
      fromUser: user,
    };

    return this.emailsService.sendEmail(preparedEmail);
  }

  @Get()
  getEmailsReceivedByUser(@Usr() user: User) {
    return this.emailsService.getEmailsReceivedByUser(user);
  }

  @Get('sent')
  getEmailsSentByUser(@Usr() user: User) {
    return this.emailsService.getEmailsSentByUser(user);
  }
}
